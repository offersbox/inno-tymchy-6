const initialState = {
  isModal: false,
  user: {}
}

export const modalReducer = (state = initialState, action) => {
  const {type, payload} = action;

  switch (type) {
    case 'CHANGE_STATUS':
      return { ...state, isModal: payload };
    case 'SET_USER':
      return { ...state, user: payload };
    default:
      return state;
  }
}
