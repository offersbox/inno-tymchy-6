const initialState = [ // пустой массив
  //   {
  //   id: 1,
  //   name: 'John Doe',
  //   email: '1@mail.ru',
  //   password: '1234',
  // }
];

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_USER':
      return [...state, action.payload];
    case 'RESULT_USES':
      return action.payload;
    case 'REMOVE_USER':
      return state.filter(user => user.id !== +action.payload);
    case 'EDIT_USER':
      return state.map(user => {
        if (user.id === action.payload.id) {
          return action.payload;
        }

        return user;
      });
    default:
      return state;
  }
}
