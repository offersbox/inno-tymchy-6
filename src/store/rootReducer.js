import { combineReducers } from 'redux';
import { userReducer } from './userReducer';
import { authReducer } from './authReducer';
import { modalReducer } from './modalReducer';

const rootReducer = combineReducers({
  users: userReducer,
  isAuth: authReducer,
  modal: modalReducer,
});

export default rootReducer;
