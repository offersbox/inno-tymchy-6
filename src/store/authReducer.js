const initialState = {
  isAuth: 
  // JSON.parse((localStorage.getItem('auth'))) || 
  false, // считывание из LS
}

console.log("localStorage.getItem('auth')", );
//  || 

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'CHANGE_AUTH':
      // localStorage.setItem('auth', action.payload); // запись в LS

      return { ...state, isAuth: action.payload };
    default:
      return state;
  }
}
