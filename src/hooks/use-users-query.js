import { useState } from "react";
import { useDispatch } from "react-redux";

export const useUsersQuery = () => {
  const [users, setUsers] = useState();
  const [isLoading, setIsLoading] = useState(false);  // состояние загрузки (спиннер)
  const dispatch = useDispatch();

  const getUsers = async () => {
    setIsLoading(true); // начинается запрос

    try {
      const response = await fetch('https://jsonplaceholder.typicode.com/users');
      const data = await response.json();

      setUsers(data);
      dispatch({ type: 'RESULT_USES', payload: data }); // запись в редьюсер
    } catch (error) {
      console.log('error')
    }

    setIsLoading(false); // начинается запрос
  }

  return { users, setUsers, getUsers, isLoading }
}
