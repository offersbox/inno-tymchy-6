import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
import './App.css';
import { UsersPage } from './pages/users/Users';
import { Home } from './pages/home/Home';
import { LayoutPage } from './pages/layout/Layout';
import { Header } from './components/header/Header';
import { Contacts } from './pages/contacts/Contacts';
import { User } from './pages/user/User';
import LoginPage from './pages/login/LoginPage';
import { useSelector } from 'react-redux';
import { Modal } from './components/modal/Modal';
import { OtherComponent } from './components/users/Users';
import { ProductPage } from './pages/product/ProductPage';

function Protected({ isSignedIn, children }) {
  if (!isSignedIn) {
    return <Navigate to="/login" replace />
  }
  return children
}

function App() {
  const authenticated = useSelector(state => state.isAuth.isAuth);
  console.log('authenticated', authenticated)

  return (
    <BrowserRouter>
      <LayoutPage>
        <Header />

        <Routes>
          <Route exact path='/login' element={<LoginPage />} />
          <Route exact path='/other' element={<OtherComponent />} />
          <Route exact path='/product/:id' element={<ProductPage />} />
          <Route
            path='/'
            element={
              <Protected isSignedIn={authenticated}>
                <Home />
              </Protected>
            }
          />
          <Route
            path="/users"
            element={
              <Protected isSignedIn={authenticated}>
                <UsersPage />
              </Protected>
            }
          />
          <Route path="/contacts" element={<Contacts />} />
          <Route
            path="/users/:id"
            element={
              <Protected isSignedIn={authenticated}>
                <User />
              </Protected>
            } />
        </Routes>

        {/* <Modal /> */}
      </LayoutPage>
    </BrowserRouter>
  )
}

export default App;
