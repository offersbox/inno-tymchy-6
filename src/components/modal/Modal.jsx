import React from 'react'
import './Modal.scss';
import { useDispatch, useSelector } from 'react-redux';

const Modal = ({isShow, children, handleClose}) => {
  // const { isModal, user } = useSelector(state => state.modal);
  // const dispatch = useDispatch();

  // const handleClose = () => {
    // dispatch({ type: 'CHANGE_STATUS', payload: false });
  // }

  return (
    <>
      {isShow && <div className='modal'>
        <div className='modal__container'>
          <div className="header">
            Title
          </div>
          <div className="body">
            {children}            
          </div>
          <div className="footer">
            <button className="button" onClick={handleClose} >Close</button>
          </div>
        </div>
      </div>}
    </>
  )
}

export { Modal }
