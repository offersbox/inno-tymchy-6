import React from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom';

const UserForm = ({ isReg }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate()

  const submit = (bool) => {
    if (bool) {

    } else {
      
    }

  }

  const change = () => {
    dispatch({ type: 'CHANGE_AUTH', payload: true })
    navigate('/')
  }

  return (
    <>
      <div>{isReg ? 'Регистрация' : 'Авторизация'}</div>
      <button onClick={() => submit(isReg)}>{isReg ? 'Зарегистрироваться' : 'Войти'}</button>
      <button onClick={change}>Change Status</button>
    </>
  )
}

export default UserForm
