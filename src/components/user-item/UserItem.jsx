import React, { useState } from 'react'
import { Button } from '../ui/Button'
import './UserItem.scss'

export const UserItem = ({ user, delUserWithId, children, onClick }) => {
  // console.log('render comp UserItem')
  const [userColor , setUserColor] = useState();

  const colors = ['red', 'green', 'blue', 'yellow', 'pink', 'black', 'orange', 'purple', 'brown', 'grey'];

  const changeColor = () => {
    const color = colors[Math.floor(Math.random() * colors.length)]

    setUserColor(color);
  }

  return (
    <div className='user' onClick={onClick}>
      {}


      <div style={{color: userColor}}>Name: {user.name}</div>
      <div>Tel: {user.phone}</div>

      {user.email && <div>Email: {user.email}</div>}

      <Button
        onClick={(e) => {
          console.log('del user')
          e.stopPropagation();
          delUserWithId(user.id)
        }}
        title='Удалить юзера'
      />

      <Button
        onClick={(e) => {
          e.stopPropagation();
          changeColor()
        }}   // #7483748, rgb(0, 0, 0)
        title='Сменить цвет'
      />

      {children}
      
      {/* <button onClick={() => delUserWithId(user.id)}>
        Новая кнопка удаления
      </button>

      <button>
        Меняет цвет текста на случайный
      </button> */}
    </div>
  )
}
