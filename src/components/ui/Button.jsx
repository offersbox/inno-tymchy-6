import React from 'react';
import './Button.scss';

// export const Button = ({ id, delUserWithId, }) => {
export const Button = ({ onClick, title, anotherClass }) => {
  // const handleDelete = (userId) => {
  //   alert(`удаляем пользователя ${userId}`)
  // }

  return (
    <>
      {/* {console.log('id', id)} */}
      <button className={anotherClass ? anotherClass : 'btn-class'} onClick={onClick}>{title}</button>
    </>
  )

}

