import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

const UserItemClick = ({ id }) => {
  const [state, setState] = useState();
  const users = useSelector(state => state.users)

  // console.log('id', id)

  useEffect(() => {
    setState(users.find(user => user.id === +id))
  }, [users])

  return (
    <>
      {/* {console.log('state', state)} */}
      {state &&
        <>
          <div>id: {state.id}</div>
          <div>name: {state.name}</div>
          <div>email: {state.email}</div>
        </>
      }
    </>
  )
}

export { UserItemClick }
