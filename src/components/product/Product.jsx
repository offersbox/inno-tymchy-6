import React, { useState } from 'react'
import { Link, useNavigate, useParams } from 'react-router-dom';

const Product = () => {
  const { id } = useParams();
  const [added, setAdded] = useState(false);
  const navigate = useNavigate();

  const arrayProducts = [{ id: 1, name: 'product1', price: 100 }, { id: 2, name: 'product2', price: 200 }, { id: 3, name: 'product3', price: 300 }];

  const findProduct = arrayProducts.find(product => product.id === Number(id));

  console.log('id', id)

  const handleAdd = () => {
    setAdded(!added)

    console.log('нажал added', added)

    if (!added) {
      console.log('добавление в корзину')
      // dispatch({ type: 'ADD_PRODUCT', payload: findProduct }) // dispatch  - {...state, product: payload}
    } else {
      console.log('удаление из корзины')
      // dispatch({ type: 'DELETE_PRODUCT', payload: id })  // dispatch  - используем filter (кроме  id)
    }


    // dispatch  payload: id/product
  }

  const handleURL = () => {
    navigate(-1)
  }

  return (
    <div>
      {console.log('state added', added)}

      <div>Name</div>
      <div>Price</div>
      <button onClick={() => handleAdd(id)}>
        {
          added
            ? <span>Добавлено</span>
            : <span>Добавить в корзину</span>
        }
      </button>


      {/* {!added
        ? <button>
        <span>Добавить в корзину</span>
        </button>
        : <div>Добавлено</div>
      } */}

      <div>
        {/* <Link to='/users'>Назад</Link> */}
        <button onClick={handleURL}>Back</button>
      </div>
    </div>
  )
}

export { Product }
