import React, { useEffect, useState } from 'react'
import { UserItem } from '../user-item/UserItem';
import './Users.scss';
import { Button } from '../ui/Button';
import { useUsersQuery } from '../../hooks/use-users-query';
import { useNavigate } from 'react-router-dom';
import { Modal } from '../modal/Modal';
import { useDispatch } from 'react-redux';
import { arrayUsers } from '../../mocks/users';

const Users = () => {
  // const { users, setUsers, getUsers, isLoading } = useUsersQuery(); // перенесли стейт, функцию запроса юзеров
  const [isModal, setIsModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const [users, setUsers] = useState([]);

  const [user, setUser] = useState(null);
  const dispatch = useDispatch();

  const navigate = useNavigate();

  const btnToDeleteTheLastUser = () => {
    const newUsersData = users.slice(0, users.length - 1);

    setUsers(newUsersData); // перезапись текущего состояния новыми данными
  }

  const delUserWithId = (id) => {
    const newUsersData = users.filter(user => user.id !== id);

    setUsers(newUsersData);
  }

  const changeUserName = (id) => {
    console.log('id', id);
    const changeUsers = users.map(user => {
      if (user.id === id) {
        return {
          ...user, // деструктуризация объекта
          name: 'Maxim'
        }
      }

      return user;
    });

    setUsers(changeUsers);
  }

  const handleChangeName = (e, id) => {
    e.stopPropagation();
    changeUserName(id)
  }

  const openModal = (user) => {
    // dispatch({ type: 'CHANGE_STATUS', payload: true });
    // dispatch({ type: 'SET_USER', payload: user });
    setUser(user);
    setIsModal(true)
  }

  const fakeQuery = (arr) => {
    return new Promise((resolve, reject) => {
      setIsLoading(true);

      setTimeout(() => {
        console.log('arr', arr);
        resolve(arr);

        setIsLoading(false);
      }, 5000)
    })

    // console.log('usersNew', usersNew);
  }

  useEffect(() => {
    // getUsers(); // из кастомного хука на строке 12
    fakeQuery(arrayUsers).then(data => {
      // console.log('data', data);
      setUsers(data);
    })
  }, [])

  return (
    <div>
      <div className='title'>Users</div>
      {users?.length && console.log('users', users)}
      {isLoading
        ? <div>Идет загрузка...</div>

        : users   // users - undefined  ==> false
          ? users.map(user => {
            return (
              <UserItem
                key={user.id} // number
                user={user}  // object
                delUserWithId={delUserWithId}
                // onClick={() => navigate(`/users/${user.id}`)}
                onClick={() => openModal(user)}
              >
                {user.id === 3
                  && <Button
                    onClick={(e) => handleChangeName(e, user.id)}
                    title='Изменить имя'
                  />}
              </UserItem>
            )
          })
          : 'пользователей нет'
      }

      {users //length === 0 --> false ;  length > 0  --> true
        // ? <button className='red' onClick={btnToDeleteTheLastUser}>Delete the Last</button>
        ? <Button
          title='Удалить последнего'
          onClick={btnToDeleteTheLastUser}
          anotherClass='red-btn'
        />
        : null
      }

      {console.log('user', user)}
      <Modal isShow={isModal} handleClose={() => setIsModal(false)}>
        <div>
          {user &&
            <>
              <div>id: {user.id}</div>
              <div>name: {user.name}</div>
              <div>email: {user.email}</div>
            </>
          }
        </div>
      </Modal>
    </div >
  )
}

export default Users


function OtherComponent() {
  return (
    <div>
      <div>OtherComponent</div>
    </div>
  )
}

export { OtherComponent }
