import React from 'react'
import './Header.scss'
import { Link, useNavigate } from 'react-router-dom'

const Header = () => {
  const navigate = useNavigate();

  const handleClick = () => {
    console.log('hello')
    navigate('/')
  }

  return (
    <div className="container">
      <Link className='link' to={`/users`}>Users</Link>
      <Link to={`/contacts`}>Contacts</Link>
      <div className='click' onClick={handleClick}>Home</div>
    </div>
  )
}

export { Header }
