import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import rootReducer from './store/rootReducer';
import { createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';


let store;
// const store = createStore(
//   rootReducer,
//   composeWithDevTools()
// );

const serializedState = localStorage.getItem('reduxState');

if (serializedState !== null) {
  const preloadedState = JSON.parse(serializedState);
  // Create the Redux store with the preloaded state
  store = createStore(rootReducer, preloadedState, composeWithDevTools());
  // ...
} else {
  // Create the Redux store without any preloaded state
  store = createStore(rootReducer, composeWithDevTools());
  // ...
}

function handleBeforeUnload() {
  const serializedState = JSON.stringify(store.getState());
  localStorage.setItem('reduxState', serializedState);
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={store} >
    <App />
  </Provider>
);

window.addEventListener('beforeunload', handleBeforeUnload); // только при перезагрузке страницы

