import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { UserItemClick } from '../../components/userOnClick/UserItemClick';
import { useDispatch, useSelector } from 'react-redux';

const User = () => {
  const { id } = useParams(); // id берется из адреса url
  
  return (
    <UserItemClick id={id} />
  )
}

export { User }
