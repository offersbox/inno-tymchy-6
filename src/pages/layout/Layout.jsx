import React from 'react'

const LayoutPage = ({ children }) => {
  return (
    <>
      {/* <div>Другой рандомный текст</div>
      <div style={{ color: 'red' }}>Еще рандомный текст</div> */}

      {children}
    </>
  )
}

export { LayoutPage }
