import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { useNavigate, Link } from 'react-router-dom';

export const Home = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [inputState, setInputState] = useState({
    login: '',
    password: ''
  });
  const [errors, setErrors] = useState({
    // login: '',
    // password: ''
  })

  function validate() {
    // const loginValue = document.getElementById('login').value;
    // const passwordValue = document.getElementById('password').value;

    if (inputState.login.length < 5) {
      setErrors(prev => ({ ...prev, login: 'Логин должен быть больше 5 символов' }))
    }
    if (inputState.password.length < 5) {
      setErrors(prev => ({  ...prev, password: 'Пароль должен быть больше 5 символов' }))
    }
  }

  // function validatePassword() {

  // }

  const handleSubmit = (e) => {
    e.preventDefault();
    validate();
    // validateLogin();
    // validatePassword();
  }

  const handleChange = (e, type) => {
    // console.log('e', e);
    if (type === 'login') {
      console.log('login', e.target.value);
      setInputState({ ...inputState, login: e.target.value })
    } else if (type === 'password') {
      console.log('password', e.target.value);
      setInputState({ ...inputState, password: e.target.value })
    }
  }

  const handleFocus = (e, type) => {
    if (type === 'login') {
      console.log('login focus');
      setErrors({ ...errors, login: '' })
    }

    if (type === 'password') {
      console.log('login focus');
      setErrors({ ...errors, password: '' })
    }
  }


  return (
    <>
      <button onClick={() => navigate('/product/5')}>
        Перейти на продукт 5
      </button>
      <button onClick={() => {
        dispatch({ type: 'CHANGE_AUTH', payload: false });
      }}>
        Выйти
      </button>
      <div style={{ marginTop: '200px' }}>

        <form action="">
          <label htmlFor="login">Логин</label>
          <input
            type="text"
            id='login'
            value={inputState.login}
            onChange={(e) => handleChange(e, 'login')}
            onFocus={(e) => handleFocus(e, 'login')}
          />

          <label htmlFor="password">Пароль</label>
          <input
            type="text"
            id='password'
            value={inputState.password}
            onChange={(e) => handleChange(e, 'password')}
            onFocus={(e) => handleFocus(e, 'password')}
          />

          <button onClick={handleSubmit}>Отправить</button>
        </form>
        {/* {console.log('errors', errors)} */}
        {errors?.login && <div style={{ color: 'red' }}>{errors.login}</div>}
        {errors?.password && <div style={{ color: 'red' }}>{errors.password}</div>}
      </div>
    </>

  )
}
