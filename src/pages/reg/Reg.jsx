import React from 'react'
import UserForm from '../../components/user-form/UserForm'

const Reg = () => {
  return (
    <UserForm isReg={true}/>
  )
}

export default Reg
