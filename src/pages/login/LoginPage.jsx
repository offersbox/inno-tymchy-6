import React from 'react'
import UserForm from '../../components/user-form/UserForm'

const LoginPage = () => {
  return (
    <UserForm isReg={false} />
  )
}

export default LoginPage
